package verticalstepperform.ernestoyaquello.com.verticalstepperform;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.models.Alarm;

/**
 * MainAbility
 *
 * @since 2021-07-14
 */
public class MainAbility extends Ability implements Component.ClickedListener {
    private static final int INT_10001 = 10001;
    private static final int INT_9 = 9;
    private static final int INT_3000 = 3000;
    private static final int RED = 65;
    private static final int GREEN = 105;
    private static final int BLUE = 225;
    private static final int INT_32 = 40;
    private static final int INT_22 = 50;
    private Text alarm;
    private Text msgToast;
    private Text mTxalarm;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.rgb(RED,GREEN,BLUE));
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_fab).setClickedListener(this);
        alarm = (Text) findComponentById(ResourceTable.Id_alarm);
        msgToast = (Text) findComponentById(ResourceTable.Id_msg_toast);
        mTxalarm = (Text) findComponentById(ResourceTable.Id_tx_alarm);
    }

    @Override
    public void onClick(Component component) {
        if (component.getId() == ResourceTable.Id_fab) {
            Intent intent = new Intent();
            Operation build = new Intent.OperationBuilder()
                    .withBundleName(getBundleName())
                    .withAbilityName(NewAlarmFormAbility.class.getName())
                    .build();
            intent.setOperation(build);
            startAbilityForResult(intent, INT_10001);
        }
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (requestCode == INT_10001 && resultCode == INT_10001) {
            if (GlobalData.getInstance().getAlarm() != null) {
                mTxalarm.setText("Don't worry, this app does not actually set up alarms.");
                mTxalarm.setTextSize(INT_32);
                alarm.setVisibility(Component.VISIBLE);
                Alarm alarmObj = GlobalData.getInstance().getAlarm();
                int hour = alarmObj.getTimeHour();
                int minutes = alarmObj.getTimeMinutes();
                String alertTime = ((hour > INT_9) ? hour : ("0" + hour)) + ":" + ((minutes > INT_9)
                        ? minutes : ("0" + minutes));
                String alertInformationText = getString(ResourceTable.String_main_activity_alarm_added_info,
                        alarmObj.getTitle(), alertTime);
                alarm.setText(alertInformationText);
                msgToast.setVisibility(Component.VISIBLE);
                getMainTaskDispatcher().delayDispatch(() ->
                        msgToast.setVisibility(Component.HIDE), INT_3000);
            } else {
                alarm.setVisibility(Component.HIDE);
                mTxalarm.setText("Click the button to set up a new alarm");
                mTxalarm.setTextSize(INT_22);
            }
        }
    }
}

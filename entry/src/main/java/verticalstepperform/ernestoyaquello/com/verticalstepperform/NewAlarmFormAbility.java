package verticalstepperform.ernestoyaquello.com.verticalstepperform;

import ernestoyaquello.com.verticalstepperform.VerticalStepperFormView;
import ernestoyaquello.com.verticalstepperform.listener.StepperFormListener;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.models.Alarm;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.steps.AlarmDaysStep;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.steps.AlarmDescriptionStep;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.steps.AlarmNameStep;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.steps.AlarmTimeStep;

/**
 * NewAlarmFormAbility
 *
 * @since 2021-07-14
 */
public class NewAlarmFormAbility extends Ability implements StepperFormListener {
    private static final int BARLENGTH = 260;
    private static final int BARWIDTH = 10;
    private static final int RIMWIDTH = 10;
    private static final int SPINSPEED = 50;
    private static final int INT_2 = 2;
    private static final int INT_3 = 3;
    private static final int INT_1000 = 1000;
    private static final int INT_100 = 100;
    private static final int INT_10001 = 10001;
    private static final int RED = 65;
    private static final int GREEN = 105;
    private static final int BLUE = 225;
    private AlarmNameStep nameStep;
    private AlarmDescriptionStep descriptionStep;
    private AlarmTimeStep timeStep;
    private AlarmDaysStep daysStep;
    private VerticalStepperFormView verticalStepperFormView;
    private DependentLayout dlDialog;
    private DependentLayout dlPwDialog;
    private ProgressWheel pw;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.rgb(RED,GREEN,BLUE));
        super.setUIContent(ResourceTable.Layout_ability_new_alarm_form);
        verticalStepperFormView = (VerticalStepperFormView) findComponentById(ResourceTable.Id_stepper_form);
        String[] stepTitles = new String[]{"Name", "Description", "Time", "Week schedule"};

        nameStep = new AlarmNameStep(stepTitles[0]);
        descriptionStep = new AlarmDescriptionStep(stepTitles[1]);
        timeStep = new AlarmTimeStep(stepTitles[INT_2],
                (DependentLayout) findComponentById(ResourceTable.Id_dl_time_picker));
        daysStep = new AlarmDaysStep(stepTitles[INT_3]);

        verticalStepperFormView
                .setup(this, nameStep, descriptionStep, timeStep, daysStep)
                .init();
        dlDialog = (DependentLayout) findComponentById(ResourceTable.Id_dl_dialog);
        Text positive = (Text) findComponentById(ResourceTable.Id_btn_positive);
        positive.setClickedListener(component -> goBack(null));
        Text negative = (Text) findComponentById(ResourceTable.Id_btn_negative);
        Image ig = (Image) findComponentById(ResourceTable.Id_ig);
        ig.setClickedListener(component -> {
            if(verticalStepperFormView.isAnyStepCompleted()) {
                dlDialog.setVisibility(Component.VISIBLE);
            } else {
                goBack(null);
            }
        });
        negative.setClickedListener(component -> {
            dlDialog.setVisibility(Component.HIDE);
            verticalStepperFormView.cancelFormCompletionOrCancellationAttempt();
        });
        DirectionalLayout dl = (DirectionalLayout) findComponentById(ResourceTable.Id_dl);
        dl.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

            }
        });
        dlPwDialog = (DependentLayout) findComponentById(ResourceTable.Id_dl_progress_dialog);
        pw = (ProgressWheel) findComponentById(ResourceTable.Id_pw);
        pw.setBarColor(Color.getIntColor("#01814A"));
        pw.setRimColor(Color.getIntColor("#ffffff"));
        pw.setBarLength(BARLENGTH);
        pw.setBarWidth(BARWIDTH);
        pw.setRimWidth(RIMWIDTH);
        pw.setSpinSpeed(SPINSPEED);
    }

    @Override
    public void onCompletedForm() {
        dlPwDialog.setVisibility(Component.VISIBLE);
        pw.startSpinning();
        verticalStepperFormView.cancelFormCompletionOrCancellationAttempt();
        final Thread dataSavingThread = saveData();
        dlPwDialog.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    dataSavingThread.interrupt();
                } catch (RuntimeException e) {
                    // No need to do anything here
                } finally {
                    verticalStepperFormView.cancelFormCompletionOrCancellationAttempt();
                    pw.stopSpinning();
                    dlPwDialog.setVisibility(Component.HIDE);
                }
            }
        });
    }

    private Thread saveData() {
        // Fake data saving effect
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(INT_1000);
                sendAlarmDataBack();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        thread.start();
        return thread;
    }

    private void sendAlarmDataBack() {
        getMainTaskDispatcher().delayDispatch(new Runnable() {
            @Override
            public void run() {
                Alarm alarm = new Alarm(
                        nameStep.getStepData(),
                        descriptionStep.getStepData(),
                        timeStep.getStepData().hour,
                        timeStep.getStepData().minutes,
                        daysStep.getStepData());
                goBack(alarm);
            }
        }, INT_100);
    }

    private void goBack(Alarm alarm) {
        Intent intent = new Intent();
        setResult(INT_10001, intent);
        GlobalData.getInstance().setAlarm(alarm);
        terminateAbility();
    }

    @Override
    public void onCancelledForm() {
        dlDialog.setVisibility(Component.VISIBLE);
    }
}

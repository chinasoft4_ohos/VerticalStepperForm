package verticalstepperform.ernestoyaquello.com.verticalstepperform.models;

import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Alarm
 *
 * @since 2021-07-14
 */
public class Alarm implements Serializable {
    private final String title;
    private final String description;
    private final int timeHour;
    private final int timeMinutes;
    private final boolean[] weekDays;

    /**
     * Alarm
     *
     * @param title
     * @param description
     * @param timeHour
     * @param timeMinutes
     * @param weekDays
     */
    public Alarm(String title, String description, int timeHour, int timeMinutes, boolean[] weekDays) {
        this.title = title;
        this.description = description;
        this.timeHour = timeHour;
        this.timeMinutes = timeMinutes;
        this.weekDays = weekDays.clone();
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getTimeHour() {
        return timeHour;
    }

    public int getTimeMinutes() {
        return timeMinutes;
    }

    /**
     * serialize
     *
     * @return String
     */
    public String serialize() {
        return new Gson().toJson(this);
    }

    /**
     * fromSerialized
     *
     * @param alarmSerialized
     * @return Alarm
     */
    public static Alarm fromSerialized(String alarmSerialized) {
        return new Gson().fromJson(alarmSerialized, Alarm.class);
    }

    public boolean[] getWeekDays() {
        return weekDays.clone();
    }
}

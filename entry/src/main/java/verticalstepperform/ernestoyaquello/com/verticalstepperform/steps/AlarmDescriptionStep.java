package verticalstepperform.ernestoyaquello.com.verticalstepperform.steps;

import ernestoyaquello.com.verticalstepperform.Step;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.ResourceTable;

/**
 * AlarmDescriptionStep
 *
 * @since 2021-07-14
 */
public class AlarmDescriptionStep extends Step<String> {
    private static final int INT_50 = 50;
    private static final int INT_10 = 10;
    private static final int INT_192 = 192;
    private static final int INT_800 = 800;
    private static final int INT_255 = 255;
    private static final int INT_165 = 165;
    private static final int INT_0 = 0;

    private TextField alarmDescriptionEditText;

    /**
     * AlarmDescriptionStep
     *
     * @param title
     */
    public AlarmDescriptionStep(String title) {
        this(title, "");
    }

    /**
     * AlarmDescriptionStep
     *
     * @param title
     * @param subtitle
     */
    public AlarmDescriptionStep(String title, String subtitle) {
        super(title, subtitle);
    }

    @Override
    protected Component createStepContentLayout() {
        // We create this step view programmatically
        alarmDescriptionEditText = new TextField(getContext());
        alarmDescriptionEditText.setWidth(INT_800);
        ShapeElement shapeElement = new ShapeElement();
        RgbColor[] rgbColors = {RgbColor.fromArgbInt(
                Color.rgb(INT_192, INT_192, INT_192)),
                RgbColor.fromArgbInt(Color.rgb(INT_192, INT_192, INT_192))};
        ShapeElement shapeElement1 = new ShapeElement();
        shapeElement1.setCornerRadius(0);
        alarmDescriptionEditText.setBubbleElement(shapeElement1);
        shapeElement.setRgbColors(rgbColors);
        alarmDescriptionEditText.setCursorElement(shapeElement);
        alarmDescriptionEditText.setBasement(shapeElement);
        alarmDescriptionEditText.setTextSize(INT_50);
        alarmDescriptionEditText.setMarginTop(INT_10);
        alarmDescriptionEditText.setHint(getContext().getString(ResourceTable.String_form_hint_description));
        alarmDescriptionEditText.setMultipleLine(false);
        alarmDescriptionEditText.setFocusChangedListener(new Component.FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean b) {
                if (b) {
                    RgbColor[] rgbColors = {RgbColor.fromArgbInt(
                            Color.rgb(INT_255, INT_165, INT_0)),
                            RgbColor.fromArgbInt(Color.rgb(INT_255, INT_165, INT_0))};
                    shapeElement.setRgbColors(rgbColors);

                } else {
                    RgbColor[] rgbColors = {RgbColor.fromArgbInt(
                            Color.rgb(INT_192, INT_192, INT_192)),
                            RgbColor.fromArgbInt(Color.rgb(INT_192, INT_192, INT_192))};
                    shapeElement.setRgbColors(rgbColors);

                }
                alarmDescriptionEditText.setCursorElement(shapeElement);
                alarmDescriptionEditText.setBasement(shapeElement);
            }
        });
        return alarmDescriptionEditText;
    }

    @Override
    protected void onStepOpened(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepClosed(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepMarkedAsCompleted(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepMarkedAsUncompleted(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    public String getStepData() {
        String text = alarmDescriptionEditText.getText();
        if (text != null) {
            return text;
        }

        return "";
    }

    @Override
    public String getStepDataAsHumanReadableString() {
        String description = getStepData();
        return description == null || description.isEmpty()
                ? getContext().getString(ResourceTable.String_form_empty_field)
                : description;
    }

    @Override
    public void restoreStepData(String data) {
        if (alarmDescriptionEditText != null) {
            alarmDescriptionEditText.setText(data);
        }
    }

    @Override
    protected IsDataValid isStepDataValid(String stepData) {
        return new IsDataValid(true);
    }
}
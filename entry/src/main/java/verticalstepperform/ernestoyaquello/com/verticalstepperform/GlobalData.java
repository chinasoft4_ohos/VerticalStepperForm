/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package verticalstepperform.ernestoyaquello.com.verticalstepperform;

import ohos.app.Context;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.models.Alarm;

/**
 * GlobalData
 *
 * @since 2021-04-25
 */
public class GlobalData {
    private static volatile GlobalData sInstance;
    private Context context;
    private Alarm alarm;

    private GlobalData() {
    }

    /**
     * getInstance
     *
     * @return GlobalData
     */
    public static GlobalData getInstance() {
        if (sInstance == null) {
            synchronized (GlobalData.class) {
                if (sInstance == null) {
                    sInstance = new GlobalData();
                }
            }
        }
        return sInstance;
    }

    /**
     * init
     *
     * @param initContext initContext
     */
    public void init(Context initContext) {
        this.context = initContext;
    }

    public Context getContext() {
        return context;
    }

    public Alarm getAlarm() {
        return alarm;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }
}

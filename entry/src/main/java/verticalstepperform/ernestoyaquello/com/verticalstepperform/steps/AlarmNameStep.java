package verticalstepperform.ernestoyaquello.com.verticalstepperform.steps;

import ernestoyaquello.com.verticalstepperform.Step;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.TextField;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.ResourceTable;

/**
 * AlarmNameStep
 *
 * @since 2021-07-14
 */
public class AlarmNameStep extends Step<String> {
    private static final int INT_50 = 50;
    private static final int INT_10 = 10;
    private static final int INT_192 = 192;
    private static final int INT_800 = 800;
    private static final int INT_255 = 255;
    private static final int INT_165 = 165;
    private static final int INT_0 = 0;
    private static final int INT_3 = 3;

    private static final int MIN_CHARACTERS_ALARM_NAME = 3;

    private TextField alarmNameEditText;
    private String unformattedErrorString;

    /**
     * AlarmNameStep
     *
     * @param title
     */
    public AlarmNameStep(String title) {
        this(title, "");
    }

    /**
     * AlarmNameStep
     *
     * @param title
     * @param subtitle
     */
    public AlarmNameStep(String title, String subtitle) {
        super(title, subtitle);
    }

    @Override
    protected Component createStepContentLayout() {
        // We create this step view programmatically
        alarmNameEditText = new TextField(getContext());
        alarmNameEditText.setWidth(INT_800);
        ShapeElement shapeElement = new ShapeElement();
        RgbColor[] rgbColors = {RgbColor.fromArgbInt(
                Color.rgb(INT_192, INT_192, INT_192)),
                RgbColor.fromArgbInt(Color.rgb(INT_192, INT_192, INT_192))};
        ShapeElement shapeElement1 = new ShapeElement();
        shapeElement1.setCornerRadius(0);
        alarmNameEditText.setBubbleElement(shapeElement1);
        shapeElement.setRgbColors(rgbColors);
        alarmNameEditText.setCursorElement(shapeElement);
        alarmNameEditText.setBasement(shapeElement);
        alarmNameEditText.setTextSize(INT_50);
        alarmNameEditText.setMarginTop(INT_10);
        alarmNameEditText.setHint(getContext().getString(ResourceTable.String_form_hint_title));
        alarmNameEditText.setMultipleLine(false);
        alarmNameEditText.addTextObserver((s, i, i1, i2) -> markAsCompletedOrUncompleted(true));

        alarmNameEditText.setFocusChangedListener(new Component.FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean b) {
                if (b) {
                    RgbColor[] rgbColors = {RgbColor.fromArgbInt(
                            Color.rgb(INT_255, INT_165, INT_0)),
                            RgbColor.fromArgbInt(Color.rgb(INT_255, INT_165, INT_0))};
                    shapeElement.setRgbColors(rgbColors);

                } else {
                    RgbColor[] rgbColors = {RgbColor.fromArgbInt(
                            Color.rgb(INT_192, INT_192, INT_192)),
                            RgbColor.fromArgbInt(Color.rgb(INT_192, INT_192, INT_192))};
                    shapeElement.setRgbColors(rgbColors);

                }
                alarmNameEditText.setCursorElement(shapeElement);
                alarmNameEditText.setBasement(shapeElement);
            }
        });
        unformattedErrorString = getContext().getString(ResourceTable.String_error_alarm_name_min_characters);

        return alarmNameEditText;
    }

    @Override
    protected void onStepOpened(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepClosed(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepMarkedAsCompleted(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepMarkedAsUncompleted(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    public String getStepData() {
        String text = alarmNameEditText.getText();
        if (text != null) {
            return text;
        }

        return "";
    }

    @Override
    public String getStepDataAsHumanReadableString() {
        String name = getStepData();
        return name == null || name.isEmpty()
                ? getContext().getString(ResourceTable.String_form_empty_field)
                : name;
    }

    @Override
    public void restoreStepData(String data) {
        if (alarmNameEditText != null) {
            alarmNameEditText.setText(data);
        }
    }

    @Override
    protected IsDataValid isStepDataValid(String stepData) {
        if (stepData.length() < MIN_CHARACTERS_ALARM_NAME) {
            String titleError = String.format(unformattedErrorString, MIN_CHARACTERS_ALARM_NAME);
            return new IsDataValid(false, titleError);
        } else {
            return new IsDataValid(true);
        }
    }
}

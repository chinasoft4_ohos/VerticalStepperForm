package verticalstepperform.ernestoyaquello.com.verticalstepperform.steps;

import java.io.IOException;

import ernestoyaquello.com.verticalstepperform.ColorUtils;
import ernestoyaquello.com.verticalstepperform.Step;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.ResourceTable;

/**
 * AlarmDaysStep
 *
 * @since 2021-07-14
 */
public class AlarmDaysStep extends Step<boolean[]> {
    private static final int INT_5 = 5;
    private static final int INT_7 = 7;
    private final String[] weekDayStrings = new String[]{"Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday"};
    private final String[] weekDays = new String[]{"M","T","W","T","F","S","S"};
    private final int[] dayResIds = new int[]{ResourceTable.Id_day_0, ResourceTable.Id_day_1, ResourceTable.Id_day_2, ResourceTable.Id_day_3,
        ResourceTable.Id_day_4,ResourceTable.Id_day_5,ResourceTable.Id_day_6};
    private boolean[] alarmDays;
    private Component daysStepContent;

    /**
     * AlarmDaysStep
     *
     * @param title
     */
    public AlarmDaysStep(String title) {
        this(title, "");
    }

    /**
     * AlarmDaysStep
     *
     * @param title
     * @param subtitle
     */
    public AlarmDaysStep(String title, String subtitle) {
        super(title, subtitle);
    }

    @Override
    protected Component createStepContentLayout() {
        // We create this step view by inflating an XML layout
        LayoutScatter inflater = LayoutScatter.getInstance(getContext());
        daysStepContent = inflater.parse(ResourceTable.Layout_step_days_of_week_layout, null, false);
        setupAlarmDays();
        return daysStepContent;
    }

    @Override
    protected void onStepOpened(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepClosed(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepMarkedAsCompleted(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepMarkedAsUncompleted(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    public boolean[] getStepData() {
        return alarmDays.clone();
    }

    @Override
    public String getStepDataAsHumanReadableString() {
        StringBuilder selectedWeekDayString = new StringBuilder();
        for (int i = 0; i < weekDayStrings.length; i++) {
            if (alarmDays[i]) {
                if (selectedWeekDayString.toString().equals("")) {
                    selectedWeekDayString = new StringBuilder(weekDayStrings[i]);
                } else {
                    selectedWeekDayString.append(",").append(weekDayStrings[i]);
                }
            }
        }
        return selectedWeekDayString.toString();
    }

    @Override
    public void restoreStepData(boolean[] data) {
    }

    @Override
    protected IsDataValid isStepDataValid(boolean[] stepData) {
        boolean thereIsAtLeastOneDaySelected = false;
        for (boolean stepDatum : stepData) {
            if (stepDatum) {
                thereIsAtLeastOneDaySelected = true;
                break;
            }
        }

        return thereIsAtLeastOneDaySelected
                ? new IsDataValid(true)
                : new IsDataValid(false, getContext().getString(ResourceTable.String_error_alarm_days_min_days));
    }

    private void setupAlarmDays() {
        boolean firstSetup = alarmDays == null;
        alarmDays = firstSetup ? new boolean[INT_7] : alarmDays;
        for (int i = 0; i < weekDays.length; i++) {
            final int index = i;
            Text dayLayout = getDayLayout(index);
            if (firstSetup) {
                // By default, we only mark the working days as activated
                alarmDays[index] = index < INT_5;
            }

            updateDayLayout(index, dayLayout, false);

            if (dayLayout != null) {
                dayLayout.setClickedListener(v -> {
                    alarmDays[index] = !alarmDays[index];
                    updateDayLayout(index, dayLayout, true);
                    markAsCompletedOrUncompleted(true);
                });

                dayLayout.setText(weekDays[index]);
            }
        }
    }

    private Text getDayLayout(int i) {
        return (Text)daysStepContent.findComponentById(dayResIds[i]);
    }

    private void updateDayLayout(int dayIndex, Text dayLayout, boolean useAnimations) {
        if (alarmDays[dayIndex]) {
            markAlarmDay(dayIndex, dayLayout, useAnimations);
        } else {
            unmarkAlarmDay(dayIndex, dayLayout, useAnimations);
        }
    }

    private void markAlarmDay(int dayIndex, Text dayLayout, boolean useAnimations) {
        alarmDays[dayIndex] = true;
        if (dayLayout != null) {
            ResourceManager resManager = getContext().getResourceManager();
            ShapeElement shapeElement = (ShapeElement)dayLayout.getBackgroundElement();
            try {
                int rgbColor = resManager.getElement(ResourceTable.Color_colorPrimary).getColor();
                shapeElement.setRgbColor(new RgbColor(ColorUtils.redInt(rgbColor),
                        ColorUtils.greenInt(rgbColor),
                        ColorUtils.blueInt(rgbColor)));
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
            dayLayout.setBackground(shapeElement);
            dayLayout.setTextColor(Color.WHITE);
        }
    }

    private void unmarkAlarmDay(int dayIndex, Text dayLayout, boolean useAnimations) {
        alarmDays[dayIndex] = false;
        ShapeElement shapeElement = (ShapeElement)dayLayout.getBackgroundElement();
        ResourceManager resManager = getContext().getResourceManager();
        shapeElement.setRgbColor(new RgbColor(0,0,0,0));
        try {
            int rgbColor = resManager.getElement(ResourceTable.Color_colorPrimary).getColor();
            dayLayout.setTextColor(new Color(rgbColor));
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        dayLayout.setBackground(shapeElement);
    }
}

package verticalstepperform.ernestoyaquello.com.verticalstepperform;

import ohos.aafwk.ability.AbilityPackage;

/**
 * MyApplication
 *
 * @since 2021-07-14
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}

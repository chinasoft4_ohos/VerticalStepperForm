package verticalstepperform.ernestoyaquello.com.verticalstepperform.steps;

import ernestoyaquello.com.verticalstepperform.Step;
import ohos.agp.components.*;
import verticalstepperform.ernestoyaquello.com.verticalstepperform.ResourceTable;

/**
 * AlarmTimeStep
 *
 * @since 2021-07-14
 */
public class AlarmTimeStep extends Step<AlarmTimeStep.TimeHolder> {
    private static final int DEFAULT_HOURS = 8;
    private static final int DEFAULT_MINUTES = 30;
    private static final int INT_9 = 9;

    private Text alarmTimeTextView;
    private TimePicker alarmTimePicker;
    private DependentLayout dlAlarmTimePicker;

    private int alarmTimeHour;
    private int alarmTimeMinutes;

    /**
     * AlarmTimeStep
     *
     * @param title
     * @param dlTimePicker
     */
    public AlarmTimeStep(String title,DependentLayout dlTimePicker) {
        this(title, "");
        dlAlarmTimePicker = dlTimePicker;
        alarmTimePicker = (TimePicker)dlAlarmTimePicker.findComponentById(ResourceTable.Id_time_picker);
        dlAlarmTimePicker.setClickedListener(component -> dlAlarmTimePicker.setVisibility(Component.HIDE));
        Text btnSure = (Text) dlAlarmTimePicker.findComponentById(ResourceTable.Id_btn_sure);
        btnSure.setClickedListener(component -> {
            alarmTimeHour = alarmTimePicker.getHour();
            alarmTimeMinutes = alarmTimePicker.getMinute();
            updatedAlarmTimeText();
            dlAlarmTimePicker.setVisibility(Component.HIDE);
        });
    }

    /**
     * AlarmTimeStep
     *
     * @param title
     */
    public AlarmTimeStep(String title) {
        this(title, "");
    }

    /**
     * AlarmTimeStep
     *
     * @param title
     * @param subtitle
     */
    public AlarmTimeStep(String title, String subtitle) {
        super(title, subtitle);
        alarmTimeHour = DEFAULT_HOURS;
        alarmTimeMinutes = DEFAULT_MINUTES;
    }

    @Override
    protected Component createStepContentLayout() {
        // We create this step view by inflating an XML layout
        LayoutScatter inflater = LayoutScatter.getInstance(getContext());
        Component timeStepContent = inflater.parse(ResourceTable.Layout_step_time_layout, null, false);
        alarmTimeTextView = (Text)timeStepContent.findComponentById(ResourceTable.Id_time);
        setupAlarmTime();
        return timeStepContent;
    }

    private void setupAlarmTime() {
        alarmTimePicker.setHour(alarmTimeHour);
        alarmTimePicker.setMinute(alarmTimeMinutes);

        if (alarmTimeTextView != null) {
            alarmTimeTextView.setClickedListener(component -> dlAlarmTimePicker.setVisibility(Component.VISIBLE));
        }
    }

    @Override
    protected void onStepOpened(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepClosed(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepMarkedAsCompleted(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    protected void onStepMarkedAsUncompleted(boolean isAnimated) {
        // No need to do anything here
    }

    @Override
    public TimeHolder getStepData() {
        return new TimeHolder(alarmTimeHour, alarmTimeMinutes);
    }

    @Override
    public String getStepDataAsHumanReadableString() {
        String hourString = ((alarmTimeHour > INT_9)
                ? String.valueOf(alarmTimeHour) : ("0" + alarmTimeHour));
        String minutesString = ((alarmTimeMinutes > INT_9)
                ? String.valueOf(alarmTimeMinutes) : ("0" + alarmTimeMinutes));
        return hourString + ":" + minutesString;
    }

    @Override
    public void restoreStepData(TimeHolder data) {
        alarmTimeHour = data.hour;
        alarmTimeMinutes = data.minutes;
        alarmTimePicker.setMinute(alarmTimeMinutes);
        alarmTimePicker.setHour(alarmTimeHour);
        updatedAlarmTimeText();
    }

    @Override
    protected IsDataValid isStepDataValid(TimeHolder stepData) {
        return new IsDataValid(true);
    }

    private void updatedAlarmTimeText() {
        alarmTimeTextView.setText(getStepDataAsHumanReadableString());
    }

    /**
     * TimeHolder
     *
     * @since 2021-07-14
     */
    public static class TimeHolder {
        /**
         * hour
         *
         */
        public int hour;
        /**
         * minutes
         */
        public int minutes;

        /**
         * TimeHolder
         *
         * @param hour
         * @param minutes
         */
        public TimeHolder(int hour, int minutes) {
            this.hour = hour;
            this.minutes = minutes;
        }
    }
}

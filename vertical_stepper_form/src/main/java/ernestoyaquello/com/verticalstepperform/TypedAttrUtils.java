/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ernestoyaquello.com.verticalstepperform;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.NoSuchElementException;

/**
 * TypedAttrUtils
 *
 * @since 2021-04-16
 */
public final class TypedAttrUtils {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "TypedAttrUtils");

    /**
     * getIntColor
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return IntColor
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue().getValue();
        }
    }

    /**
     * getColor
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return Color
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue();
        }
    }

    /**
     * getBoolean
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param isDefValue isDefValue
     * @return boolean
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean isDefValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return isDefValue;
        } else {
            return attr.getBoolValue();
        }
    }

    /**
     * getString
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return String
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    /**
     * getDimensionPixelSize
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static int getDimensionPixelSize(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static int getLayoutDimension(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getDimensionValue();
        }
    }

    /**
     * getInteger
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static int getInteger(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * getFloat
     *
     * @param attrs attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return float
     */
    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getFloatValue();
        }
    }

    /**
     * attrNoSuchElement
     *
     * @param attrs attrs
     * @param attrName attrName
     * @return Attr
     */
    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        try {
            attr = attrs.getAttr(attrName).get();
        } catch (NoSuchElementException e) {
            HiLog.info(LABEL, "Exception = " + e.toString());
        }
        return attr;
    }

    /**
     * getFont
     *
     * @param context context
     * @param fontFileName fontFileName
     * @return Font
     */
    public static Font getFont(Context context, String fontFileName) {
        ResourceManager resManager = context.getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry("resources/rawfile/" + fontFileName);
        Resource resource;
        try {
            resource = rawFileEntry.openRawFile();
        } catch (IOException e) {
            return Font.DEFAULT;
        }
        File file = new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), fontFileName);
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            int index;
            byte[] bytes = new byte[1024];
            while ((index = resource.read(bytes)) != -1) {
                outputStream .write(bytes, 0, index);
                outputStream .flush();
            }
        } catch (IOException e) {
            return Font.DEFAULT;
        } finally {
            try {
                resource.close();
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                return Font.DEFAULT;
            }
        }
        Font.Builder builder = new Font.Builder(file);
        return builder.build();
    }
}

package ernestoyaquello.com.verticalstepperform;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import ernestoyaquello.com.verticalstepperform.listener.StepperFormListener;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Custom layout that implements a vertical stepper form.
 *
 * @since 2021-07-14
 */
public class VerticalStepperFormView extends DirectionalLayout {
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "VerticalStepperFormView");
    private static final float FLOAT_3 = 0.3f;
    private static final float FLOAT_2 = 0.2f;
    private static final int INT_1 = -1;
    private static final int INT_2 = 2;
    FormStepListener internalListener;
    FormStyle style;

    private StepperFormListener listener;
    private KeyboardTogglingObserver keyboardTogglingObserver;
    private List<StepHelper> stepHelpers;
    private List<StepHelper> originalStepHelpers;
    private boolean isNnitialized;

    private DirectionalLayout formContentView;
    private ProgressBar progressBar;
    private DependentLayout dlPreviousStepButton;
    private DependentLayout dlNextStepButton;
    private Image previousStepButton;
    private Image nextStepButton;
    private Component bottomNavigationView;

    private boolean isFormCompleted;
    private boolean isKeyboardIsOpen;

    /**
     * VerticalStepperFormView
     *
     * @param context
     * @param attrs
     */
    public VerticalStepperFormView(Context context, AttrSet attrs) {
        this(context, attrs, "");
    }

    /**
     * VerticalStepperFormView
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public VerticalStepperFormView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        onConstructed(context, attrs, defStyleAttr);
    }

    /**
     * Gets an instance of the builder that will be used to set up and initialize the form.
     *
     * @param stepperFormListener The listener for the stepper form events.
     * @param steps An array with the steps that will be displayed in the form.
     * @return An instance of the stepper form builder. Use it to configure and initialize the form.
     */
    public Builder setup(StepperFormListener stepperFormListener, Step<?>... steps) {
        return new Builder(this, stepperFormListener, steps);
    }

    /**
     * Gets an instance of the builder that will be used to set up and initialize the form.
     *
     * @param stepperFormListener The listener for the stepper form events.
     * @param steps A list with the steps that will be displayed in the form.
     * @return An instance of the stepper form builder. Use it to configure and initialize the form.
     */
    public Builder setup(StepperFormListener stepperFormListener, List<Step<?>> steps) {
        Step<?>[] stepsArray = steps.toArray(new Step<?>[0]);
        return new Builder(this, stepperFormListener, stepsArray);
    }

    /**
     * Marks the currently open step as completed or uncompleted depending on whether the step data
     * is valid or not.
     *
     * @param isUseAnimations True to animate the changes in the views, false to not.
     * @return True if the step was found and marked as completed; false otherwise.
     */
    public synchronized boolean markOpenStepAsCompletedOrUncompleted(boolean isUseAnimations) {
        return markStepAsCompletedOrUncompleted(getOpenStepPosition(), isUseAnimations);
    }

    /**
     * Marks the specified step as completed or uncompleted depending on whether the step data is
     * valid or not.
     *
     * @param stepPosition The step position.
     * @param isUseAnimations True to animate the changes in the views, false to not.
     * @return True if the step was found and marked as completed; false otherwise.
     */
    public boolean markStepAsCompletedOrUncompleted(int stepPosition, boolean isUseAnimations) {
        if (stepPosition >= 0 && stepPosition < stepHelpers.size()) {
            StepHelper stepHelper = stepHelpers.get(stepPosition);
            return stepHelper.getStepInstance().markAsCompletedOrUncompleted(isUseAnimations);
        }

        return false;
    }

    /**
     * Marks the currently open step as completed.
     *
     * @param isUseAnimations True to animate the changes in the views, false to not.
     */
    public synchronized void markOpenStepAsCompleted(boolean isUseAnimations) {
        markStepAsCompleted(getOpenStepPosition(), isUseAnimations);
    }

    /**
     * Marks the specified step as completed.
     *
     * @param stepPosition The step position.
     * @param isUseAnimations True to animate the changes in the views, false to not.
     */
    public void markStepAsCompleted(int stepPosition, boolean isUseAnimations) {
        if (stepPosition >= 0 && stepPosition < stepHelpers.size()) {
            StepHelper stepHelper = stepHelpers.get(stepPosition);
            stepHelper.getStepInstance().markAsCompleted(isUseAnimations);
        }
    }

    /**
     * Marks the currently open step as uncompleted.
     *
     * @param errorMessage The error message.
     * @param isUseAnimations True to animate the changes in the views, false to not.
     */
    public synchronized void markOpenStepAsUncompleted(boolean isUseAnimations, String errorMessage) {
        markStepAsUncompleted(getOpenStepPosition(), errorMessage, isUseAnimations);
    }

    /**
     * Marks the specified step as uncompleted.
     *
     * @param stepPosition The step position.
     * @param errorMessage The error message.
     * @param isUseAnimations True to animate the changes in the views, false to not.
     */
    public void markStepAsUncompleted(int stepPosition, String errorMessage, boolean isUseAnimations) {
        if (stepPosition >= 0 && stepPosition < stepHelpers.size()) {
            StepHelper stepHelper = stepHelpers.get(stepPosition);
            stepHelper.getStepInstance().markAsUncompleted(errorMessage, isUseAnimations);
        }
    }

    /**
     * Determines whether the open step is marked as completed or not.
     *
     * @return True if the open step is currently marked as completed; false otherwise.
     */
    public synchronized boolean isOpenStepCompleted() {
        return isStepCompleted(getOpenStepPosition());
    }

    /**
     * Determines whether the specified step is marked as completed or not.
     *
     * @param stepPosition The step position.
     * @return True if the step is currently marked as completed; false otherwise.
     */
    public boolean isStepCompleted(int stepPosition) {
        if (stepPosition >= 0 && stepPosition < stepHelpers.size()) {
            return stepHelpers.get(stepPosition).getStepInstance().isCompleted();
        }

        return false;
    }

    /**
     * Determines whether there is at least one step marked as completed.
     *
     * @return True if at least one step has been marked as completed; false otherwise.
     */
    public boolean isAnyStepCompleted() {
        for (StepHelper stepHelper : stepHelpers) {
            if (stepHelper.getStepInstance().isCompleted()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines whether all the steps previous to the specified one are currently marked as completed.
     *
     * @param stepPosition The step position.
     * @return True if all the steps previous to the specified one are marked as completed; false otherwise.
     */
    public boolean areAllPreviousStepsCompleted(int stepPosition) {
        boolean isPreviousStepsAreCompleted = true;
        for (int i = stepPosition - 1; i >= 0; i--) {
            isPreviousStepsAreCompleted &= stepHelpers.get(i).getStepInstance().isCompleted();
        }

        return isPreviousStepsAreCompleted;
    }

    /**
     * Determines whether all the steps are currently marked as completed.
     *
     * @return True if all the steps are marked as completed; false otherwise.
     */
    public boolean areAllStepsCompleted() {
        return areAllPreviousStepsCompleted(stepHelpers.size());
    }

    /**
     * Determines whether the form has already been completed or cancelled.
     * Please note that this could return false even if all the steps are completed (for example,
     * if the user has filled in all the required data but hasn't submitted the form yet).
     *
     * @return True if the form has been completed or cancelled; false otherwise.
     */
    public boolean isFormCompleted() {
        return isFormCompleted;
    }

    /**
     * If possible, goes to the step that is positioned after the currently open one, closing the
     * current one and opening the next one.
     * Please note that, unless isAllowNonLinearNavigation is set to true, it will only be possible to
     * navigate to a certain step if all the previous ones are marked as completed.
     *
     * @param isUseAnimations Indicates whether or not the affected steps will be opened/closed using
     * animations.
     *
     * @return True if the navigation to the step was performed; false otherwise.
     */
    public synchronized boolean goToNextStep(boolean isUseAnimations) {
        return goToStep(getOpenStepPosition() + 1, isUseAnimations);
    }

    /**
     * If possible, goes to the step that is positioned before the currently open one, closing the
     * current one and opening the previous one.
     * Please note that, unless isAllowNonLinearNavigation is set to true, it will only be possible to
     * navigate to a certain step if all the previous ones are marked as completed.
     *
     * @param isUseAnimations Indicates whether or not the affected steps will be opened/closed using animations.
     * @return True if the navigation to the step was performed; false otherwise.
     */
    public synchronized boolean goToPreviousStep(boolean isUseAnimations) {
        return goToStep(getOpenStepPosition() - 1, isUseAnimations);
    }

    /**
     * If possible, goes to the specified step, closing the currently open one and opening the
     * target one.
     * Please note that, unless isAllowNonLinearNavigation is set to true, it will only be possible to
     * navigate to a certain step if all the previous ones are marked as completed.
     * In case the navigation is possible and the specified position to go to is the last one + 1,
     * the form will attempt to complete.
     *
     * @param stepPosition The step position to go to. If it is the next one to the actual last one,
     * the form will attempt to complete.
     *
     * @param isUseAnimations Indicates whether or not the affected steps will be opened/closed using
     * animations.
     *
     * @return True if the navigation to the step was performed; false otherwise.
     */
    public synchronized boolean goToStep(int stepPosition, boolean isUseAnimations) {
        if (isFormCompleted) {
            return false;
        }

        int openStepPosition = getOpenStepPosition();
        if (openStepPosition != stepPosition && stepPosition >= 0 && stepPosition <= stepHelpers.size()) {
            boolean previousStepsAreCompleted = areAllPreviousStepsCompleted(stepPosition);
            if ((style.isAllowNonLinearNavigation && stepPosition < stepHelpers.size()) || previousStepsAreCompleted) {
                openStep(stepPosition, isUseAnimations);

                return true;
            }
        }

        return false;
    }

    /**
     * Gets the position of the currently open step.
     *
     * @return The position of the currently open step, counting from 0. -1 if not found.
     */
    public synchronized int getOpenStepPosition() {
        for (int i = 0; i < stepHelpers.size(); i++) {
            StepHelper stepHelper = stepHelpers.get(i);
            if (stepHelper.getStepInstance().isOpen()) {
                return i;
            }
        }

        return INT_1;
    }

    /**
     * Gets the currently open step.
     *
     * @return The currently open step, or null if not found.
     */
    public synchronized Step<?> getOpenStep() {
        StepHelper openStepHelper = getOpenStepHelper();
        return openStepHelper != null ? openStepHelper.getStepInstance() : null;
    }

    /**
     * Gets the content layout of the specified step (i.e., the layout which was provided at start
     * to setup the step).
     *
     * @param stepPosition The step position.
     * @return If found, the layout. If not, null.
     */
    public Component getStepContentLayout(int stepPosition) {
        if (stepPosition >= 0 && stepPosition < stepHelpers.size()) {
            return stepHelpers.get(stepPosition).getStepInstance().getContentLayout();
        }

        return null;
    }

    /**
     * Shows the bottom navigation bar.
     */
    public void showBottomNavigation() {
        bottomNavigationView.setVisibility(Component.VISIBLE);
    }

    /**
     * Hides the bottom navigation bar.
     */
    public void hideBottomNavigation() {
        bottomNavigationView.setVisibility(Component.HIDE);
    }

    /**
     * Scrolls to the top of the specified step, but only in case its content is not visible.
     *
     * @param stepPosition The step position.
     * @param isSmoothScroll Determines whether the scrolling should be smooth or abrupt.
     */
    public void scrollToStepIfNecessary(final int stepPosition, final boolean isSmoothScroll) {
        if (stepPosition >= 0 && stepPosition < stepHelpers.size()) {
            HiLog.info(LABEL, "scrollToStepIfNecessary");
        }
    }

    /**
     * Scrolls to the top of the currently open step, but only in case its content is not visible.
     *
     * @param isSmoothScroll Determines whether the scrolling should be smooth or abrupt.
     */
    public synchronized void scrollToOpenStepIfNecessary(boolean isSmoothScroll) {
        scrollToStepIfNecessary(getOpenStepPosition(), isSmoothScroll);
    }

    /**
     * If all the steps are currently marked as completed, completes the form, disabling the step
     * navigation and the button(s) of the last step, and invoking onCompletedForm() on the listener.
     * To revert these changes (for example, because saving or sending the data has failed and you
     * want the form to go back to normal so the user can use it), call
     * cancelFormCompletionOrCancellationAttempt().
     */
    public void completeForm() {
        attemptToCompleteForm(false);
    }

    /**
     * Cancels the form, disabling the step navigation and the button(s) of the currently open step,
     * and invoking onCancelledForm() on the listener.
     * To revert these changes (for example, because the user has dismissed the cancellation and you
     * want the form to go back to normal), call cancelFormCompletionOrCancellationAttempt().
     */
    public void cancelForm() {
        attemptToCompleteForm(true);
    }

    /**
     * To be used after a failed form completion attempt or after a dismissed cancellation attempt,
     * this method re-activates the navigation to other steps and re-enables the button(s) of the
     * currently open step.
     * Useful when saving the form data fails and you want to allow the user to use the form again
     * in order to re-send the data.
     */
    public synchronized void cancelFormCompletionOrCancellationAttempt() {
        if (!isFormCompleted) {
            return;
        }

        int openedStepPosition = getOpenStepPosition();
        openedStepPosition = openedStepPosition == INT_1 ? stepHelpers.size() - 1 : openedStepPosition;
        StepHelper stepHelper = stepHelpers.get(openedStepPosition);

        if (style.isCloseLastStepOnCompletion) {
            Step<?> step = stepHelper.getStepInstance();
            if (!step.isOpen()) {
                step.openInternal(true);
            }
        }

        if ((openedStepPosition + 1) < stepHelpers.size() || areAllStepsCompleted()) {
            stepHelper.enableAllButtons();
        } else {
            stepHelper.enableCancelButton();
        }

        isFormCompleted = false;
        updateBottomNavigationButtons();
    }

    /**
     * Refreshes the progress bar of the bottom navigation depending on the number of steps marked
     * as completed, returning the number of completed steps.
     *
     * @return The number of steps that are currently marked as completed.
     */
    public int refreshFormProgress() {
        int numberOfCompletedSteps = 0;
        for (StepHelper stepHelper : stepHelpers) {
            if (stepHelper.getStepInstance().isCompleted()) {
                ++numberOfCompletedSteps;
            }
        }
        setProgress(numberOfCompletedSteps);

        return numberOfCompletedSteps;
    }

    /**
     * Adds a step to the form in the specified position.
     * Please note that this change will be lost on restoration events like rotation. Hence, it
     * will be up to you to re-add the step (and its state) AFTER the form is restored without it.
     *
     * @param index The index where the step will be added.
     * @param stepToAdd The step to add.
     * @return True if the step was added successfully; false otherwise.
     */
    public boolean addStep(int index, Step<?> stepToAdd) {
        StepHelper lastStep = stepHelpers.get(stepHelpers.size() - 1);
        int lastAllowedIndex = lastStep.isConfirmationStep() ? stepHelpers.size() - 1 : stepHelpers.size();
        if (!isNnitialized || isFormCompleted() || index < 0 || index > lastAllowedIndex) {
            return false;
        }

        StepHelper stepHelper = new StepHelper(internalListener, stepToAdd);
        stepHelpers.add(index, stepHelper);
        for (int i = 0; i < stepHelpers.size(); i++) {
            if (i != index) {
                StepHelper previouslyExistingStepHelper = stepHelpers.get(i);
                previouslyExistingStepHelper.updateStepViewsAfterPositionChange(this);
            }
        }

        stepToAdd.markAsCompletedOrUncompletedInternal(false, true);

        progressBar.setMaxValue(stepHelpers.size());
        refreshFormProgress();
        updateBottomNavigationButtons();
        enableOrDisableLastStepNextButton();
        Component stepLayout = initializeStepHelper(index);
        formContentView.addComponent(stepLayout, index);
        int openStepPosition = getOpenStepPosition();
        if (!style.isAllowNonLinearNavigation && !isStepCompleted(index) && index < openStepPosition) {
            goToStep(index, true);
        }

        return true;
    }

    /**
     * Removes the step that is placed at the specified position.
     * Please note that this change will be lost on restoration events like rotation. Hence, it
     * will be up to you to remove the step again AFTER the form is restored with it on it.
     *
     * @param index The index where the step to delete is.
     * @return True if the step was deleted successfully; false otherwise.
     */
    public boolean removeStep(int index) {
        StepHelper lastStep = stepHelpers.get(stepHelpers.size() - 1);
        int lastAllowedIndex = lastStep.isConfirmationStep() ? stepHelpers.size() - INT_2 : stepHelpers.size() - 1;
        if (!isNnitialized || isFormCompleted() || index < 0 || index > lastAllowedIndex || stepHelpers.size() <= 1) {
            return false;
        }

        stepHelpers.remove(index);
        for (StepHelper previouslyExistingStepHelper : stepHelpers) {
            previouslyExistingStepHelper.updateStepViewsAfterPositionChange(this);
        }

        progressBar.setMaxValue(stepHelpers.size());
        refreshFormProgress();
        updateBottomNavigationButtons();
        enableOrDisableLastStepNextButton();

        formContentView.removeComponentAt(index);
        int openStepPosition = getOpenStepPosition();
        int previousOpenStepPosition = getOpenStepPosition();
        if (previousOpenStepPosition != INT_1 && openStepPosition == INT_1) {
            int stepToOpen = index > 0 ? index - 1 : 0;
            goToStep(stepToOpen, true);
        }

        return true;
    }

    /**
     * Gets the total number of steps of the form.
     *
     * @return The total number of steps, including the confirmation step, if any.
     */
    public int getTotalNumberOfSteps() {
        return stepHelpers.size();
    }

    /**
     * Gets the position of the specified step within the list of steps of the form.
     *
     * @param step The step to find the position of.
     * @return The position of the step, or -1 if the step is not found.
     */
    public int getStepPosition(Step<?> step) {
        for (int i = 0; i < stepHelpers.size(); i++) {
            if (stepHelpers.get(i).getStepInstance() == step) {
                return i;
            }
        }

        return INT_1;
    }

    /**
     * onConstructed
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    private void onConstructed(Context context, AttrSet attrs, String defStyleAttr) {
        LayoutScatter inflater = LayoutScatter.getInstance(context);
        inflater.parse(ResourceTable.Layout_vertical_stepper_form_layout, this, true);
        findViews();
        keyboardTogglingObserver = new KeyboardTogglingObserver();

        style = new FormStyle();
        ResourceManager resManager = mContext.getResourceManager();

        // Set the default values for all the style properties
        try {
            style.stepNextButtonText = TypedAttrUtils.getString(attrs, "form_next_button_text",
                    resManager.getElement(ResourceTable.String_vertical_stepper_form_continue_button).getString());
            style.lastStepNextButtonText = resManager
                    .getElement(ResourceTable.String_vertical_stepper_form_confirm_button).getString();
            style.lastStepCancelButtonText = resManager
                    .getElement(ResourceTable.String_vertical_stepper_form_cancel_button).getString();
            style.confirmationStepTitle = resManager
                    .getElement(ResourceTable.String_vertical_stepper_form_confirmation_step_title).getString();
            style.confirmationStepSubtitle = "";
            style.leftCircleSizeInPx = (int) resManager
                    .getElement(ResourceTable.Float_vertical_stepper_form_width_circle).getFloat();
            style.leftCircleTextSizeInPx = (int) resManager
                    .getElement(ResourceTable.Float_vertical_stepper_form_text_size_circle).getFloat();
            style.stepTitleTextSizeInPx = (int) resManager
                    .getElement(ResourceTable.Float_vertical_stepper_form_text_size_title).getFloat();
            style.stepSubtitleTextSizeInPx = (int) resManager
                            .getElement(ResourceTable.Float_vertical_stepper_form_text_size_subtitle).getFloat();
            style.stepErrorMessageTextSizeInPx = (int) resManager
                            .getElement(ResourceTable.Float_vertical_stepper_form_text_size_error_message).getFloat();
            style.leftVerticalLineThicknessSizeInPx = (int) resManager
                            .getElement(ResourceTable.Float_vertical_stepper_form_width_vertical_line).getFloat();
            style.marginFromStepNumbersToContentInPx = (int) resManager
                            .getElement(ResourceTable.Float_vertical_stepper_form_space_between_numbers_and_content)
                            .getFloat();
            style.backgroundColorOfDisabledElements = resManager
                            .getElement(ResourceTable.Color_vertical_stepper_form_background_color_disabled_elements)
                            .getColor();
            style.stepNumberBackgroundColor =
                    resManager.getElement(ResourceTable.Color_vertical_stepper_form_background_color_circle).getColor();
            style.stepNumberCompletedBackgroundColor =
                    resManager.getElement(ResourceTable.Color_vertical_stepper_form_background_color_circle).getColor();
            style.stepNumberErrorBackgroundColor =
                    resManager.getElement(ResourceTable.Color_vertical_stepper_form_background_color_circle).getColor();
            style.nextButtonBackgroundColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_background_color_next_button).getColor();
            style.nextButtonPressedBackgroundColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_background_color_next_button_pressed)
                    .getColor();
            style.lastStepCancelButtonBackgroundColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_background_color_cancel_button).getColor();
            style.lastStepCancelButtonPressedBackgroundColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_background_color_cancel_button_pressed)
                    .getColor();
            style.stepNumberTextColor =
                    resManager.getElement(ResourceTable.Color_vertical_stepper_form_text_color_circle).getColor();
            style.stepTitleTextColor =
                    resManager.getElement(ResourceTable.Color_vertical_stepper_form_text_color_title).getColor();
            style.stepSubtitleTextColor =
                    resManager.getElement(ResourceTable.Color_vertical_stepper_form_text_color_subtitle).getColor();
            style.nextButtonTextColor =
                    resManager.getElement(ResourceTable.Color_vertical_stepper_form_text_color_next_button).getColor();
            style.nextButtonPressedTextColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_text_color_next_button_pressed)
                    .getColor();
            style.lastStepCancelButtonTextColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_text_color_cancel_button).getColor();
            style.lastStepCancelButtonPressedTextColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_text_color_cancel_button_pressed).getColor();
            style.errorMessageTextColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_text_color_error_message).getColor();
            style.bottomNavigationBackgroundColor = resManager
                    .getElement(ResourceTable.Color_vertical_stepper_form_background_color_bottom_navigation)
                    .getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        style.isDisplayBottomNavigation = true;
        style.isDisplayStepButtons = true;
        style.isDisplayCancelButtonInLastStep = false;
        style.isDisplayNextButtonInLastStep = true;
        style.isDisplayStepDataInSubtitleOfClosedSteps = true;
        style.isDisplayDifferentBackgroundColorOnDisabledElements = false;
        style.isIncludeConfirmationStep = true;
        style.isAllowNonLinearNavigation = false;
        style.isAllowStepOpeningOnHeaderClick = true;
        style.isCloseLastStepOnCompletion = false;
        style.alphaOfDisabledElements = FLOAT_3;
        style.stepNumberFontFamily = Font.DEFAULT;
        style.stepTitleFontFamily = Font.DEFAULT;
        style.stepSubtitleFontFamily = Font.DEFAULT;
        style.stepErrorMessageFontFamily = Font.DEFAULT;

        // Try to get the user values for the style properties to replace the default ones
        style.lastStepCancelButtonText = TypedAttrUtils
                .getString(attrs, "form_cancel_button_text", style.lastStepCancelButtonText);
        style.confirmationStepTitle = TypedAttrUtils
                .getString(attrs, "form_confirmation_step_title_text", style.confirmationStepTitle);
//        style.confirmationStepSubtitle = TypedAttrUtils
//                .getString(attrs, "form_confirmation_step_subtitle_text", style.confirmationStepTitle);
        style.leftCircleSizeInPx = TypedAttrUtils
                .getInteger(attrs, "form_circle_size", style.leftCircleSizeInPx);
        style.leftCircleTextSizeInPx = TypedAttrUtils
                .getInteger(attrs, "form_circle_text_size", style.leftCircleTextSizeInPx);
        style.stepTitleTextSizeInPx = TypedAttrUtils
                .getInteger(attrs, "form_title_text_size", style.stepTitleTextSizeInPx);
        style.stepSubtitleTextSizeInPx = TypedAttrUtils
                .getInteger(attrs, "form_subtitle_text_size", style.stepSubtitleTextSizeInPx);
        style.stepErrorMessageTextSizeInPx = TypedAttrUtils
                .getInteger(attrs, "form_error_message_text_size", style.stepErrorMessageTextSizeInPx);
        style.leftVerticalLineThicknessSizeInPx = TypedAttrUtils
                .getInteger(attrs, "form_vertical_line_width", style.leftVerticalLineThicknessSizeInPx);
        style.marginFromStepNumbersToContentInPx = TypedAttrUtils
                .getInteger(attrs, "form_horizontal_margin_from_step_numbers_to_content",
                        style.marginFromStepNumbersToContentInPx);
        style.backgroundColorOfDisabledElements = TypedAttrUtils
                .getIntColor(attrs, "form_disabled_elements_background_color",
                        style.backgroundColorOfDisabledElements);
        style.stepNumberBackgroundColor = TypedAttrUtils
                .getIntColor(attrs, "form_disabled_elements_background_color",
                        style.stepNumberBackgroundColor);
        style.stepNumberCompletedBackgroundColor = TypedAttrUtils
                .getIntColor(attrs, "form_circle_completed_background_color",
                        style.stepNumberCompletedBackgroundColor);
        style.stepNumberErrorBackgroundColor = TypedAttrUtils
                .getIntColor(attrs, "form_circle_error_background_color",
                        style.stepNumberErrorBackgroundColor);
        style.nextButtonBackgroundColor = TypedAttrUtils
                .getIntColor(attrs, "form_next_button_background_color", style.stepNumberErrorBackgroundColor);
        style.nextButtonPressedBackgroundColor = TypedAttrUtils
                .getIntColor(attrs, "form_next_button_pressed_background_color",
                        style.nextButtonPressedBackgroundColor);
        style.lastStepCancelButtonBackgroundColor = TypedAttrUtils
                .getIntColor(attrs, "form_cancel_button_background_color",
                        style.lastStepCancelButtonBackgroundColor);
        style.lastStepCancelButtonPressedBackgroundColor = TypedAttrUtils
                .getIntColor(attrs, "form_cancel_button_pressed_background_color",
                        style.lastStepCancelButtonPressedBackgroundColor);
        style.stepNumberTextColor = TypedAttrUtils
                .getIntColor(attrs, "form_circle_text_color", style.stepNumberTextColor);
        style.stepTitleTextColor = TypedAttrUtils
                .getIntColor(attrs, "form_title_text_color", style.stepTitleTextColor);
        style.stepSubtitleTextColor = TypedAttrUtils
                .getIntColor(attrs, "form_subtitle_text_color", style.stepSubtitleTextColor);
        style.nextButtonTextColor = TypedAttrUtils
                .getIntColor(attrs, "form_next_button_text_color", style.nextButtonTextColor);
        style.nextButtonPressedTextColor = TypedAttrUtils
                .getIntColor(attrs, "form_next_button_pressed_text_color", style.nextButtonPressedTextColor);
        style.lastStepCancelButtonTextColor = TypedAttrUtils
                .getIntColor(attrs, "form_cancel_button_text_color", style.lastStepCancelButtonTextColor);
        style.lastStepCancelButtonPressedTextColor = TypedAttrUtils
                .getIntColor(attrs, "form_cancel_button_pressed_text_color",
                        style.lastStepCancelButtonPressedTextColor);
        style.errorMessageTextColor = TypedAttrUtils
                .getIntColor(attrs, "form_error_message_text_color", style.errorMessageTextColor);
        style.bottomNavigationBackgroundColor = TypedAttrUtils
                .getIntColor(attrs, "form_bottom_navigation_background_color",
                        style.bottomNavigationBackgroundColor);
        style.isDisplayBottomNavigation = TypedAttrUtils
                .getBoolean(attrs, "form_display_bottom_navigation", style.isDisplayBottomNavigation);
        style.isDisplayStepButtons = TypedAttrUtils
                .getBoolean(attrs, "form_display_step_buttons", style.isDisplayStepButtons);
        style.isDisplayCancelButtonInLastStep = TypedAttrUtils
                .getBoolean(attrs, "form_display_cancel_button_in_last_step",
                        style.isDisplayCancelButtonInLastStep);
        style.isDisplayNextButtonInLastStep = TypedAttrUtils
                .getBoolean(attrs, "form_display_next_button_in_last_step",
                        style.isDisplayNextButtonInLastStep);
        style.isDisplayStepDataInSubtitleOfClosedSteps = TypedAttrUtils
                .getBoolean(attrs, "form_display_step_data_in_subtitle_of_closed_steps",
                        style.isDisplayStepDataInSubtitleOfClosedSteps);
        style.isDisplayDifferentBackgroundColorOnDisabledElements = TypedAttrUtils
                .getBoolean(attrs, "form_display_different_background_color_on_disabled_elements",
                        style.isDisplayDifferentBackgroundColorOnDisabledElements);
        style.isIncludeConfirmationStep = TypedAttrUtils
                .getBoolean(attrs, "form_include_confirmation_step", style.isIncludeConfirmationStep);
        style.isAllowNonLinearNavigation = TypedAttrUtils
                .getBoolean(attrs, "form_allow_non_linear_navigation", style.isAllowNonLinearNavigation);
        style.isAllowStepOpeningOnHeaderClick = TypedAttrUtils
                .getBoolean(attrs, "form_allow_step_opening_on_header_click",
                        style.isAllowStepOpeningOnHeaderClick);
        style.isCloseLastStepOnCompletion = TypedAttrUtils
                .getBoolean(attrs, "form_close_last_step_on_completion", style.isCloseLastStepOnCompletion);
        style.alphaOfDisabledElements = TypedAttrUtils
                .getFloat(attrs, "form_alpha_of_disabled_elements", style.alphaOfDisabledElements);
        String stepNumberFontFamilyString = TypedAttrUtils
                .getString(attrs, "form_step_number_font_family", "");
        style.stepNumberFontFamily = TypedAttrUtils
                .getFont(context, stepNumberFontFamilyString);
        String stepTitleFontFamilyString = TypedAttrUtils
                .getString(attrs, "form_step_title_font_family", "");
        style.stepTitleFontFamily = TypedAttrUtils
                .getFont(context, stepTitleFontFamilyString);
        String stepSubtitleFontFamilyString = TypedAttrUtils
                .getString(attrs, "form_step_subtitle_font_family", "");
        style.stepSubtitleFontFamily = TypedAttrUtils
                .getFont(context, stepSubtitleFontFamilyString);
        String stepErrorMessageFontFamilyString = TypedAttrUtils
                .getString(attrs, "form_step_error_message_font_family", "");
        style.stepErrorMessageFontFamily = TypedAttrUtils.getFont(context, stepErrorMessageFontFamilyString);

        internalListener = new FormStepListener();
    }

    void setListener(StepperFormListener stepperFormListener) {
        listener = stepperFormListener;
    }

    StepperFormListener getListener() {
        return listener;
    }

    void initializeForm(StepperFormListener listener, StepHelper[] stepsArray) {
        setListener(listener);
        this.originalStepHelpers = Arrays.asList(stepsArray);
        this.stepHelpers = new ArrayList<>(originalStepHelpers);

        progressBar.setMaxValue(stepHelpers.size());

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(ColorUtils.redInt(style.bottomNavigationBackgroundColor),
                ColorUtils.greenInt(style.bottomNavigationBackgroundColor),
                ColorUtils.blueInt(style.bottomNavigationBackgroundColor)));
        bottomNavigationView.setBackground(shapeElement);
        if (!style.isDisplayBottomNavigation) {
            hideBottomNavigation();
        }

        for (int i = 0; i < stepHelpers.size(); i++) {
            Component stepLayout = initializeStepHelper(i);
            formContentView.addComponent(stepLayout);
        }

        goToStep(0, false);

        isNnitialized = true;
    }

    private Component initializeStepHelper(int position) {
        StepHelper stepHelper = stepHelpers.get(position);
        boolean isLast = (position + 1) == stepHelpers.size();
        int stepLayoutResourceId = getStepLayoutResourceId(position, isLast);

        return stepHelper.initialize(this, formContentView, stepLayoutResourceId);
    }

    /**
     * getStepLayoutResourceId
     *
     * @param position
     * @param isLast
     * @return int
     */
    protected int getStepLayoutResourceId(int position, boolean isLast) {
        // This could be overridden to use a custom step layout
        return ResourceTable.Layout_step_layout;
    }

    private StepHelper getOpenStepHelper() {
        for (StepHelper stepHelper : stepHelpers) {
            if (stepHelper.getStepInstance().isOpen()) {
                return stepHelper;
            }
        }
        return null;
    }

    private synchronized void openStep(int stepToOpenPosition, boolean isUseAnimations) {
        if (stepToOpenPosition >= 0 && stepToOpenPosition < stepHelpers.size()) {
            int stepToClosePosition = getOpenStepPosition();
            if (stepToClosePosition != INT_1) {
                StepHelper stepToClose = stepHelpers.get(stepToClosePosition);
                stepToClose.getStepInstance().closeInternal(isUseAnimations);
            }

            StepHelper stepToOpen = stepHelpers.get(stepToOpenPosition);
            stepToOpen.getStepInstance().openInternal(isUseAnimations);
        } else if (stepToOpenPosition == stepHelpers.size()) {
            attemptToCompleteForm(false);
        }
    }

    /**
     * updateBottomNavigationButtons
     *
     */
    protected synchronized void updateBottomNavigationButtons() {
        int stepPosition = getOpenStepPosition();
        if (stepPosition >= 0 && stepPosition < stepHelpers.size()) {
            StepHelper stepHelper = stepHelpers.get(stepPosition);

            if (!isFormCompleted && stepPosition > 0) {
                enablePreviousButtonInBottomNavigation();
            } else {
                disablePreviousButtonInBottomNavigation();
            }

            if (!isFormCompleted
                    && (stepPosition + 1) < stepHelpers.size()
                    && (style.isAllowNonLinearNavigation || stepHelper.getStepInstance().isCompleted())) {
                enableNextButtonInBottomNavigation();
            } else {
                disableNextButtonInBottomNavigation();
            }
        }
    }

    /**
     * disablePreviousButtonInBottomNavigation
     *
     */
    protected void disablePreviousButtonInBottomNavigation() {
        disableBottomButtonNavigation(previousStepButton);
    }

    /**
     * enablePreviousButtonInBottomNavigation
     *
     */
    protected void enablePreviousButtonInBottomNavigation() {
        enableBottomButtonNavigation(previousStepButton);
    }

    /**
     * disableNextButtonInBottomNavigation
     *
     */
    protected void disableNextButtonInBottomNavigation() {
        disableBottomButtonNavigation(nextStepButton);
    }

    /**
     * enableNextButtonInBottomNavigation
     *
     */
    protected void enableNextButtonInBottomNavigation() {
        enableBottomButtonNavigation(nextStepButton);
    }

    private void enableBottomButtonNavigation(Component button) {
        button.setAlpha(1f);
        button.setEnabled(true);
    }

    private void disableBottomButtonNavigation(Component button) {
        button.setAlpha(style.alphaOfDisabledElements);
        button.setEnabled(false);
    }

    private void setProgress(int numberOfCompletedSteps) {
        if (numberOfCompletedSteps >= 0 && numberOfCompletedSteps <= stepHelpers.size()) {
            progressBar.setProgressValue(numberOfCompletedSteps);
        }
    }

    private void enableOrDisableLastStepNextButton() {
        if (!areAllStepsCompleted()) {
            stepHelpers.get(stepHelpers.size() - 1).disableNextButton();
        } else {
            stepHelpers.get(stepHelpers.size() - 1).enableNextButton();
        }
    }

    private synchronized void attemptToCompleteForm(boolean isCancellation) {
        if (isFormCompleted) {
            return;
        }

        // If the last step is a confirmation step that happens to be marked as uncompleted,
        // here we attempt to mark it as completed so the form can be completed
        boolean isMarkedConfirmationStepAsCompleted = false;
        String confirmationStepErrorMessage = "";
        StepHelper lastStepHelper = stepHelpers.get(stepHelpers.size() - 1);
        Step<?> lastStep = lastStepHelper.getStepInstance();
        if (!isCancellation) {
            if (!lastStep.isCompleted() && lastStepHelper.isConfirmationStep()) {
                confirmationStepErrorMessage = lastStep.getErrorMessage();
                lastStep.markAsCompletedOrUncompleted(true);
                if (lastStep.isCompleted()) {
                    isMarkedConfirmationStepAsCompleted = true;
                }
            }
        }

        int openStepPosition = getOpenStepPosition();
        if (openStepPosition >= 0 && openStepPosition < stepHelpers.size()
                && (isCancellation || areAllStepsCompleted())) {
            isFormCompleted = true;
            stepHelpers.get(openStepPosition).disableAllButtons();
            updateBottomNavigationButtons();

            if (getListener() != null) {
                if (!isCancellation) {
                    getListener().onCompletedForm();
                } else {
                    getListener().onCancelledForm();
                }
            }
        } else if (isMarkedConfirmationStepAsCompleted) {
            // If the completion attempt fails, we restore the confirmation step to its previous state
            lastStep.markAsUncompleted(confirmationStepErrorMessage, true);
        }

        if (!isCancellation && style.isCloseLastStepOnCompletion) {
            lastStep.closeInternal(true);
        }
    }

    private void findViews() {
        formContentView = (DirectionalLayout) findComponentById(ResourceTable.Id_content);
        progressBar = (ProgressBar) findComponentById(ResourceTable.Id_progress_bar);
        dlPreviousStepButton = (DependentLayout) findComponentById(ResourceTable.Id_dl_down_previous);
        dlNextStepButton = (DependentLayout) findComponentById(ResourceTable.Id_dl_down_next);
        previousStepButton = (Image) findComponentById(ResourceTable.Id_down_previous);
        nextStepButton = (Image) findComponentById(ResourceTable.Id_down_next);
        bottomNavigationView = findComponentById(ResourceTable.Id_bottom_navigation);
        registerListeners();
    }

    private void registerListeners() {
        previousStepButton.setClickedListener(component -> goToPreviousStep(true));
        nextStepButton.setClickedListener(component -> goToNextStep(true));
        dlPreviousStepButton.setClickedListener(component -> goToPreviousStep(true));
        dlNextStepButton.setClickedListener(component -> goToNextStep(true));
        addObserverForKeyboard();
    }

    private void addObserverForKeyboard() {
        isKeyboardIsOpen = isKeyboardOpen();
        setLayoutRefreshedListener(keyboardTogglingObserver);
    }

    private boolean isKeyboardOpen() {
        Rect rect = new Rect();
        formContentView.getWindowVisibleRect(rect);
        Optional<Display> optional = DisplayManager.getInstance().getDefaultDisplay(mContext);
        int screenHeight = optional.get().getRealAttributes().height;
        int keyboardHeight = screenHeight - rect.bottom;
        return keyboardHeight > screenHeight * FLOAT_2;
    }

    /**
     * FormStepListener
     *
     * @since 2021-07-14
     */
    class FormStepListener implements Step.InternalFormStepListener {
        @Override
        public void onUpdatedTitle(int stepPosition, boolean isUseAnimations) {
            // No need to do anything here
        }

        @Override
        public void onUpdatedSubtitle(int stepPosition, boolean isUseAnimations) {
            // No need to do anything here
        }

        @Override
        public void onUpdatedButtonText(int stepPosition, boolean isUseAnimations) {
            // No need to do anything here
        }

        @Override
        public void onUpdatedErrorMessage(int stepPosition, boolean isUseAnimations) {
            // No need to do anything here
        }

        @Override
        public void onUpdatedStepCompletionState(int stepPosition, boolean isUseAnimations) {
            updateBottomNavigationButtons();
            refreshFormProgress();
            enableOrDisableLastStepNextButton();
        }

        @Override
        public void onUpdatedStepVisibility(int stepPosition, boolean isUseAnimations) {
            updateBottomNavigationButtons();
            scrollToOpenStepIfNecessary(isUseAnimations);
            enableOrDisableLastStepNextButton();
        }
    }

    /**
     * FormStyle
     *
     * @since 2021-07-14
     */
    static class FormStyle {
        String stepNextButtonText;
        String lastStepNextButtonText;
        String lastStepCancelButtonText;
        String confirmationStepTitle;
        String confirmationStepSubtitle;
        int leftCircleSizeInPx;
        int leftCircleTextSizeInPx;
        int stepTitleTextSizeInPx;
        int stepSubtitleTextSizeInPx;
        int stepErrorMessageTextSizeInPx;
        int leftVerticalLineThicknessSizeInPx;
        int marginFromStepNumbersToContentInPx;
        int backgroundColorOfDisabledElements;
        int stepNumberBackgroundColor;
        int stepNumberCompletedBackgroundColor;
        int stepNumberErrorBackgroundColor;
        int nextButtonBackgroundColor;
        int nextButtonPressedBackgroundColor;
        int lastStepCancelButtonBackgroundColor;
        int lastStepCancelButtonPressedBackgroundColor;
        int stepNumberTextColor;
        int stepTitleTextColor;
        int stepSubtitleTextColor;
        int nextButtonTextColor;
        int nextButtonPressedTextColor;
        int lastStepCancelButtonTextColor;
        int lastStepCancelButtonPressedTextColor;
        int errorMessageTextColor;
        int bottomNavigationBackgroundColor;
        boolean isDisplayBottomNavigation;
        boolean isDisplayStepButtons;
        boolean isDisplayCancelButtonInLastStep;
        boolean isDisplayNextButtonInLastStep;
        boolean isDisplayStepDataInSubtitleOfClosedSteps;
        boolean isDisplayDifferentBackgroundColorOnDisabledElements;
        boolean isIncludeConfirmationStep;
        boolean isAllowNonLinearNavigation;
        boolean isAllowStepOpeningOnHeaderClick;
        boolean isCloseLastStepOnCompletion;
        float alphaOfDisabledElements;
        Font stepNumberFontFamily;
        Font stepTitleFontFamily;
        Font stepSubtitleFontFamily;
        Font stepErrorMessageFontFamily;
    }

    /**
     * KeyboardTogglingObserver
     *
     * @since 2021-07-14
     */
    private class KeyboardTogglingObserver implements Component.LayoutRefreshedListener {
        @Override
        public void onRefreshed(Component component) {
            boolean isKeyboardWasOpen = isKeyboardIsOpen;
            isKeyboardIsOpen = isKeyboardOpen();
            if (isNnitialized && isKeyboardIsOpen != isKeyboardWasOpen) {
                scrollToOpenStepIfNecessary(true);
            }
        }
    }
}
package ernestoyaquello.com.verticalstepperform;

import ernestoyaquello.com.verticalstepperform.VerticalStepperFormView.FormStyle;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * This class holds a step instance and deals with updating its views so they reflect its state.
 * It can also handle the logic of the optional confirmation step.
 *
 * All this logic could certainly be in the base class of the step, but by keeping it here we make
 * that class cleaner, making it easier to understand for anyone taking a look at it.
 *
 * @since 2021-07-14
 */
class StepHelper implements Step.InternalFormStepListener {
    private static final int INT_255 = 255;
    private Step<?> step;
    private FormStyle formStyle;

    private Component stepNumberCircleView;
    private Text titleView;
    private Text subtitleView;
    private Text stepNumberTextView;
    private Image doneIconView;
    private Text errorMessageView;
    private Component headerView;
    private Button nextButtonView;
    private Button cancelButtonView;
    private Component lineView1;
    private Component lineView2;
    private Component stepAndButtonView;
    private Component errorMessageContainerView;
    private Component titleAndSubtitleContainerView;
    private Component errorContentAndButtonContainerView;

    StepHelper(Step.InternalFormStepListener formListener, Step<?> step) {
        this(formListener, step, false);
    }

    StepHelper(Step.InternalFormStepListener formListener, Step<?> step, boolean isConfirmationStep) {
        this.step = !isConfirmationStep ? step : new ConfirmationStep();
        this.step.addListenerInternal(this);
        this.step.addListenerInternal(formListener);
    }

    Component initialize(VerticalStepperFormView form, ComponentContainer parent, int stepLayoutResourceId) {
        if (step.getEntireStepLayout() == null) {
            formStyle = form.style;

            Context context = form.getContext();
            LayoutScatter inflater = LayoutScatter.getInstance(context);
            Component stepLayout = inflater.parse(stepLayoutResourceId, parent, false);

            step.initializeStepInternal(stepLayout, form);
            step.setContentLayoutInternal(step.createStepContentLayout());

            setupStepViews(form, stepLayout);
        } else {
            throw new IllegalStateException("This step has already been initialized");
        }

        return step.getEntireStepLayout();
    }

    /**
     * setupStepViews
     *
     * @param form
     * @param stepLayout
     */
    private void setupStepViews(final VerticalStepperFormView form, Component stepLayout) {
        if (step.getContentLayout() != null) {
            ComponentContainer contentContainerLayout = (ComponentContainer) step.getEntireStepLayout()
                    .findComponentById(ResourceTable.Id_step_content);
            contentContainerLayout.addComponent(step.getContentLayout());
        }

        stepNumberCircleView = stepLayout.findComponentById(ResourceTable.Id_step_number_circle);
        stepNumberTextView = (Text) stepLayout.findComponentById(ResourceTable.Id_step_number);
        titleView = (Text) stepLayout.findComponentById(ResourceTable.Id_step_title);
        subtitleView = (Text) stepLayout.findComponentById(ResourceTable.Id_step_subtitle);
        doneIconView = (Image) stepLayout.findComponentById(ResourceTable.Id_step_done_icon);
        errorMessageView = (Text) stepLayout.findComponentById(ResourceTable.Id_step_error_message);
        headerView = stepLayout.findComponentById(ResourceTable.Id_step_header);
        nextButtonView = (Button) stepLayout.findComponentById(ResourceTable.Id_step_button);
        cancelButtonView = (Button) stepLayout.findComponentById(ResourceTable.Id_step_cancel_button);
        lineView1 = stepLayout.findComponentById(ResourceTable.Id_line1);
        lineView2 = stepLayout.findComponentById(ResourceTable.Id_line2);
        stepAndButtonView = step.getEntireStepLayout().findComponentById(ResourceTable.Id_step_content_and_button);
        errorMessageContainerView = step.getEntireStepLayout().findComponentById(ResourceTable.Id_step_error_container);
        titleAndSubtitleContainerView = step.getEntireStepLayout()
                .findComponentById(ResourceTable.Id_title_subtitle_container);
        errorContentAndButtonContainerView = step.getEntireStepLayout()
                .findComponentById(ResourceTable.Id_error_content_button_container);

        stepNumberTextView.setFont(formStyle.stepNumberFontFamily);
        titleView.setFont(formStyle.stepTitleFontFamily);
        subtitleView.setFont(formStyle.stepSubtitleFontFamily);
        errorMessageView.setFont(formStyle.stepErrorMessageFontFamily);

        titleView.setTextColor(new Color(formStyle.stepTitleTextColor));
        subtitleView.setTextColor(new Color(formStyle.stepSubtitleTextColor));
        stepNumberTextView.setTextColor(new Color(formStyle.stepNumberTextColor));
        errorMessageView.setTextColor(new Color(formStyle.errorMessageTextColor));

        ShapeElement circleShape = new ShapeElement(form.getContext(),ResourceTable.Graphic_circle_step_done);
        circleShape.setRgbColor(new RgbColor(ColorUtils.redInt(formStyle.stepNumberBackgroundColor),
                ColorUtils.greenInt(formStyle.stepNumberBackgroundColor),
                ColorUtils.blueInt(formStyle.stepNumberBackgroundColor)));
        stepNumberCircleView.setBackground(circleShape);

        UIHelper.setButtonColor(
                nextButtonView,
                formStyle.nextButtonBackgroundColor,
                formStyle.nextButtonTextColor,
                formStyle.nextButtonPressedBackgroundColor,
                formStyle.nextButtonPressedTextColor,
                (float) (Color.alpha(formStyle.nextButtonBackgroundColor) / INT_255));
        UIHelper.setButtonColor(
                cancelButtonView,
                formStyle.lastStepCancelButtonBackgroundColor,
                formStyle.lastStepCancelButtonTextColor,
                formStyle.lastStepCancelButtonPressedBackgroundColor,
                formStyle.lastStepCancelButtonPressedTextColor,
                (float) (Color.alpha(formStyle.lastStepCancelButtonBackgroundColor) / INT_255));

        ComponentContainer.LayoutConfig layoutParamsCircle = stepNumberCircleView.getLayoutConfig();
        layoutParamsCircle.width = formStyle.leftCircleSizeInPx;
        layoutParamsCircle.height = formStyle.leftCircleSizeInPx;
        stepNumberCircleView.setLayoutConfig(layoutParamsCircle);

        ComponentContainer.LayoutConfig layoutParamsLine1 = lineView1.getLayoutConfig();
        layoutParamsLine1.width = formStyle.leftVerticalLineThicknessSizeInPx;
        lineView1.setLayoutConfig(layoutParamsLine1);

        ComponentContainer.LayoutConfig layoutParamsLine2 = lineView2.getLayoutConfig();
        layoutParamsLine2.width = formStyle.leftVerticalLineThicknessSizeInPx;
        lineView2.setLayoutConfig(layoutParamsLine2);

        DirectionalLayout.LayoutConfig titleAndSubtitleContainerLayoutParams =
                (DirectionalLayout.LayoutConfig) titleAndSubtitleContainerView.getLayoutConfig();
        titleAndSubtitleContainerLayoutParams.setMarginLeft(formStyle.marginFromStepNumbersToContentInPx);
        titleAndSubtitleContainerView.setLayoutConfig(titleAndSubtitleContainerLayoutParams);

        DirectionalLayout.LayoutConfig errorContentAndButtonContainerLayoutParams =
                (DirectionalLayout.LayoutConfig) errorContentAndButtonContainerView.getLayoutConfig();
        errorContentAndButtonContainerLayoutParams.setMarginLeft(formStyle.marginFromStepNumbersToContentInPx);
        errorContentAndButtonContainerView.setLayoutConfig(errorContentAndButtonContainerLayoutParams);

        stepNumberTextView.setTextSize(formStyle.leftCircleTextSizeInPx, Text.TextSizeType.PX);
        titleView.setTextSize(formStyle.stepTitleTextSizeInPx, Text.TextSizeType.PX);
        subtitleView.setTextSize(formStyle.stepSubtitleTextSizeInPx, Text.TextSizeType.PX);
        errorMessageView.setTextSize(formStyle.stepErrorMessageTextSizeInPx, Text.TextSizeType.PX);

        headerView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (formStyle.isAllowStepOpeningOnHeaderClick) {
                    form.goToStep(form.getStepPosition(step), true);
                }
            }
        });
        nextButtonView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                form.goToStep(form.getStepPosition(step) + 1, true);
            }
        });
        cancelButtonView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                form.cancelForm();
            }
        });

        int position = form.getStepPosition(step);

        String title = !isConfirmationStep()
                ? step.getTitle()
                : formStyle.confirmationStepTitle;
        String subtitle = !isConfirmationStep()
                ? step.getSubtitle()
                : formStyle.confirmationStepSubtitle;
        stepNumberTextView.setText(String.valueOf(position + 1));
        step.updateTitle(title, false);
        step.updateSubtitle(subtitle, false);
        boolean isLast = (position + 1) == form.getTotalNumberOfSteps();
        String stepNextButtonText = !step.getNextButtonText().isEmpty()
                ? step.getNextButtonText()
                : isLast ? formStyle.lastStepNextButtonText : formStyle.stepNextButtonText;
        step.updateNextButtonText(stepNextButtonText, false);

        if (formStyle.isDisplayCancelButtonInLastStep && isLast) {
            String cancelButtonText = formStyle.lastStepCancelButtonText == null
                    ? "" : formStyle.lastStepCancelButtonText;
            cancelButtonView.setText(cancelButtonText);
            cancelButtonView.setVisibility(Component.VISIBLE);
        }

        if (!formStyle.isDisplayNextButtonInLastStep && isLast) {
            nextButtonView.setVisibility(Component.HIDE);
        }

        if (!formStyle.isDisplayStepButtons && !isConfirmationStep()) {
            nextButtonView.setVisibility(Component.HIDE);
        }

        if (isLast) {
            lineView1.setVisibility(Component.HIDE);
            lineView2.setVisibility(Component.HIDE);
        }

        onUpdatedStepCompletionState(position, false);
        onUpdatedStepVisibility(position, false);
    }

    void updateStepViewsAfterPositionChange(VerticalStepperFormView form) {
        int position = form.getStepPosition(step);
        boolean isLast = (position + 1) == form.getTotalNumberOfSteps();

        stepNumberTextView.setText(String.valueOf(position + 1));

        String stepNextButtonText = !step.getOriginalNextButtonText().isEmpty()
                ? step.getOriginalNextButtonText()
                : isLast ? formStyle.lastStepNextButtonText : formStyle.stepNextButtonText;
        step.updateNextButtonText(stepNextButtonText, false);

        if (formStyle.isDisplayCancelButtonInLastStep && isLast) {
            String cancelButtonText = formStyle.lastStepCancelButtonText == null
                    ? "" : formStyle.lastStepCancelButtonText;
            cancelButtonView.setText(cancelButtonText);
            cancelButtonView.setVisibility(Component.VISIBLE);
        } else {
            cancelButtonView.setVisibility(Component.HIDE);
        }

        if (formStyle.isDisplayNextButtonInLastStep && isLast) {
            String nextButtonText = formStyle.lastStepNextButtonText == null
                    ? "" : formStyle.lastStepNextButtonText;
            nextButtonView.setText(nextButtonText);
            nextButtonView.setVisibility(Component.VISIBLE);
        } else {
            nextButtonView.setVisibility(Component.HIDE);
        }

        lineView1.setVisibility(isLast ? Component.HIDE : Component.VISIBLE);
        lineView2.setVisibility(isLast ? Component.HIDE : Component.VISIBLE);

        onUpdatedStepCompletionState(position, false);
        onUpdatedStepVisibility(position, false);
    }

    public Step<?> getStepInstance() {
        return step;
    }

    @Override
    public void onUpdatedTitle(int stepPosition, boolean isUseAnimations) {
        if (step.getEntireStepLayout() != null) {
            updateTitleTextViewValue();
        }
    }

    @Override
    public void onUpdatedSubtitle(int stepPosition, boolean isUseAnimations) {
        if (step.getEntireStepLayout() != null) {
            if (updateSubtitleTextViewValue()) {
                updateSubtitleVisibility(isUseAnimations);
            }
        }
    }

    @Override
    public void onUpdatedButtonText(int stepPosition, boolean isUseAnimations) {
        if (step.getEntireStepLayout() != null) {
            updateButtonTextValue();
        }
    }

    @Override
    public void onUpdatedErrorMessage(int stepPosition, boolean isUseAnimations) {
        if (step.getEntireStepLayout() != null) {
            if (updateErrorMessageTextViewValue()) {
                updateErrorMessageVisibility(isUseAnimations);
            }
        }
    }

    @Override
    public void onUpdatedStepVisibility(int stepPosition, boolean isUseAnimations) {
        if (step.getEntireStepLayout() != null) {
            if (step.isOpen()) {
                UIHelper.slideDownIfNecessary(stepAndButtonView, isUseAnimations);

                // As soon as the step opens, we update its completion state
                boolean isWasCompleted = step.isCompleted();
                boolean isCompleted = step.markAsCompletedOrUncompleted(isUseAnimations);
                if (isCompleted == isWasCompleted) {
                    updateHeader(isUseAnimations);
                }
            } else {
                UIHelper.slideUpIfNecessary(stepAndButtonView, isUseAnimations);
                updateHeader(isUseAnimations);
            }
        }
    }

    @Override
    public void onUpdatedStepCompletionState(int stepPosition, boolean isUseAnimations) {
        if (step.getEntireStepLayout() != null) {
            if (step.isCompleted()) {
                enableNextButton();
            } else {
                disableNextButton();
            }
            updateHeader(isUseAnimations);
        }
    }

    private void updateHeader(boolean isUseAnimations) {
        // Update alpha of header elements
        boolean isEnableHeader = step.isOpen() || step.isCompleted();
        float alpha = isEnableHeader ? 1f : formStyle.alphaOfDisabledElements;
        float subtitleAlpha = isEnableHeader ? 1f : 0f;
        titleView.setAlpha(alpha);
        subtitleView.setAlpha(subtitleAlpha);
        stepNumberCircleView.setAlpha(alpha);

        // Update background color of left circle
        int stepNumberBackgroundColor = !step.hasError()
                ? isEnableHeader
                    ? formStyle.stepNumberCompletedBackgroundColor
                    : formStyle.stepNumberBackgroundColor
                : formStyle.stepNumberErrorBackgroundColor;
        if (formStyle.isDisplayDifferentBackgroundColorOnDisabledElements && !isEnableHeader) {
            stepNumberBackgroundColor = formStyle.backgroundColorOfDisabledElements;
        }
        ShapeElement circleShape =
                new ShapeElement(stepNumberCircleView.getContext(),ResourceTable.Graphic_circle_step_done);
        circleShape.setRgbColor(new RgbColor(ColorUtils.redInt(stepNumberBackgroundColor),
                ColorUtils.greenInt(stepNumberBackgroundColor),
                ColorUtils.blueInt(stepNumberBackgroundColor)));
        stepNumberCircleView.setBackground(circleShape);

        // Update step position circle indicator layout
        if (step.isOpen() || !step.isCompleted()) {
            showStepNumberAndHideDoneIcon();
        } else {
            showDoneIconAndHideStepNumber();
        }

        updateSubtitleTextViewValue();
        updateSubtitleVisibility(isUseAnimations);
        updateErrorMessageVisibility(isUseAnimations);
    }

    private void showDoneIconAndHideStepNumber() {
        doneIconView.setVisibility(Component.VISIBLE);
        stepNumberTextView.setVisibility(Component.HIDE);
    }

    private void showStepNumberAndHideDoneIcon() {
        doneIconView.setVisibility(Component.HIDE);
        stepNumberTextView.setVisibility(Component.VISIBLE);
    }

    void enableNextButton() {
        nextButtonView.setEnabled(true);
        if (formStyle.isDisplayDifferentBackgroundColorOnDisabledElements) {
            UIHelper.setButtonColor(
                    nextButtonView,
                    formStyle.nextButtonBackgroundColor,
                    formStyle.nextButtonTextColor,
                    formStyle.nextButtonPressedBackgroundColor,
                    formStyle.nextButtonPressedTextColor,1);
        } else {
            UIHelper.setButtonColor(
                    nextButtonView,
                    formStyle.nextButtonBackgroundColor,
                    formStyle.nextButtonTextColor,
                    0,
                    0,1);
        }
        nextButtonView.setAlpha(1);
    }

    void disableNextButton() {
        nextButtonView.setEnabled(false);
        if (formStyle.isDisplayDifferentBackgroundColorOnDisabledElements) {
            UIHelper.setButtonColor(
                    nextButtonView,
                    formStyle.backgroundColorOfDisabledElements,
                    formStyle.nextButtonTextColor,
                    formStyle.backgroundColorOfDisabledElements,
                    formStyle.nextButtonPressedTextColor,
                    formStyle.alphaOfDisabledElements);
        } else {
            UIHelper.setButtonColor(
                    nextButtonView,
                    formStyle.nextButtonBackgroundColor,
                    formStyle.nextButtonTextColor,
                    0,
                    0,
                    formStyle.alphaOfDisabledElements);
        }
    }

    void enableCancelButton() {
        cancelButtonView.setEnabled(true);
        if (formStyle.isDisplayDifferentBackgroundColorOnDisabledElements) {
            UIHelper.setButtonColor(
                    cancelButtonView,
                    formStyle.lastStepCancelButtonBackgroundColor,
                    formStyle.lastStepCancelButtonTextColor,
                    formStyle.lastStepCancelButtonPressedBackgroundColor,
                    formStyle.lastStepCancelButtonPressedTextColor,
                    (float) (Color.alpha(formStyle.lastStepCancelButtonBackgroundColor) / INT_255));
        } else {
            UIHelper.setButtonColor(
                    cancelButtonView,
                    formStyle.lastStepCancelButtonBackgroundColor,
                    formStyle.lastStepCancelButtonTextColor,
                    0,
                    0,
                    (float) (Color.alpha(formStyle.lastStepCancelButtonBackgroundColor) / INT_255));
        }
        cancelButtonView.setAlpha(1);
    }

    void disableCancelButton() {
        cancelButtonView.setEnabled(false);
        if (formStyle.isDisplayDifferentBackgroundColorOnDisabledElements) {
            UIHelper.setButtonColor(
                    cancelButtonView,
                    formStyle.backgroundColorOfDisabledElements,
                    formStyle.lastStepCancelButtonTextColor,
                    formStyle.backgroundColorOfDisabledElements,
                    formStyle.lastStepCancelButtonPressedTextColor,
                    formStyle.alphaOfDisabledElements);
        } else {
            UIHelper.setButtonColor(
                    cancelButtonView,
                    formStyle.lastStepCancelButtonBackgroundColor,
                    formStyle.lastStepCancelButtonTextColor,
                    0,
                    0,
                    formStyle.alphaOfDisabledElements);
        }
        cancelButtonView.setAlpha(formStyle.alphaOfDisabledElements);
    }

    void enableAllButtons() {
        if (step.isCompleted()) {
            enableNextButton();
        }
        enableCancelButton();
    }

    void disableAllButtons() {
        disableNextButton();
        disableCancelButton();
    }

    private boolean updateTitleTextViewValue() {
        CharSequence previousValue = titleView.getText();
        String previousValueAsString = previousValue == null ? "" : previousValue.toString();

        String title = step.getTitle();
        if (!title.equals(previousValueAsString)) {
            titleView.setText(title);
            return true;
        }

        return false;
    }

    private boolean updateSubtitleTextViewValue() {
        CharSequence previousValue = subtitleView.getText();
        String previousValueAsString = previousValue == null ? "" : previousValue.toString();

        String subtitle = getActualSubtitleText();
        if (!subtitle.equals(previousValueAsString)) {
            if (!subtitle.isEmpty()) {
                // We don't update the text view if the subtitle is empty; instead, we leave the last
                // non-empty subtitle so the text view has text and can be seen while animating to hide
                subtitleView.setText(subtitle);
            }

            return true;
        }

        return false;
    }

    private boolean updateButtonTextValue() {
        CharSequence previousValue = nextButtonView.getText();
        String previousValueAsString = previousValue == null ? "" : previousValue.toString();

        String buttonText = step.getNextButtonText();
        if (!buttonText.equals(previousValueAsString)) {
            nextButtonView.setText(buttonText);
            return true;
        }

        return false;
    }

    private boolean updateErrorMessageTextViewValue() {
        CharSequence previousValue = errorMessageView.getText();
        String previousValueAsString = previousValue == null ? "" : previousValue.toString();

        String errorMessage = step.getErrorMessage();
        if (!errorMessage.equals(previousValueAsString)) {
            if (!errorMessage.isEmpty()) {
                // We don't update the text view if the error message is empty; instead, we leave the last
                // non-empty error message so the text view has text and can be seen while animating to hide
                errorMessageView.setText(errorMessage);
            }

            return true;
        }

        return false;
    }

    private void updateSubtitleVisibility(boolean isUseAnimations) {
        boolean isShowSubtitle = !getActualSubtitleText().isEmpty()
                && (step.isOpen() || step.isCompleted());
        if (isShowSubtitle) {
            UIHelper.slideDownIfNecessary(subtitleView, isUseAnimations);
        } else {
            UIHelper.slideUpIfNecessary(subtitleView, isUseAnimations);
        }
    }

    private void updateErrorMessageVisibility(boolean isUseAnimations) {
        if (step.isOpen() && !step.isCompleted() && !step.getErrorMessage().isEmpty()) {
            UIHelper.slideDownIfNecessary(errorMessageContainerView, isUseAnimations);
        } else {
            UIHelper.slideUpIfNecessary(errorMessageContainerView, isUseAnimations);
        }
    }

    private String getActualSubtitleText() {
        String subtitle = formStyle.isDisplayStepDataInSubtitleOfClosedSteps && !step.isOpen()
                ? step.getStepDataAsHumanReadableString()
                : step.getSubtitle();
        subtitle = subtitle == null ? "" : subtitle;

        return subtitle;
    }

    boolean isConfirmationStep() {
        return step instanceof ConfirmationStep;
    }

    /**
     * This step will just display a button that the user will have to click to complete the form.
     *
     * @since 2021-07-14
     */
    private static class ConfirmationStep extends Step<Object> {
        ConfirmationStep() {
            super("");
        }

        @Override
        public Object getStepData() {
            return null;
        }

        @Override
        public String getStepDataAsHumanReadableString() {
            return getSubtitle();
        }

        @Override
        public void restoreStepData(Object data) {
            // No need to do anything here
        }

        @Override
        protected IsDataValid isStepDataValid(Object stepData) {
            return null;
        }

        @Override
        protected Component createStepContentLayout() {
            return null;
        }

        @Override
        protected void onStepOpened(boolean isAnimated) {
            // No need to do anything here
        }

        @Override
        protected void onStepClosed(boolean isAnimated) {
            if (!getFormView().isFormCompleted()) {
                markAsUncompleted("", isAnimated);
            }
        }

        @Override
        protected void onStepMarkedAsCompleted(boolean isAnimated) {
            // No need to do anything here
        }

        @Override
        protected void onStepMarkedAsUncompleted(boolean isAnimated) {
            // No need to do anything here
        }
    }
}

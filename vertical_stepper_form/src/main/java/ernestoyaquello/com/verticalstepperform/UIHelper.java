package ernestoyaquello.com.verticalstepperform;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;

/**
 * UIHelper
 *
 * @since 2021-07-14
 */
class UIHelper {
    private static final long MIN_DURATION_MILLIS = 150;
    private static final int INT_255 = 255;

    private static Map<Component, AnimatorValue> runningObjectAnimators = new ConcurrentHashMap<>();

    /**
     * setButtonColor
     *
     * @param button
     * @param buttonColor
     * @param buttonTextColor
     * @param buttonPressedColor
     * @param buttonPressedTextColor
     * @param alpha
     */
    static void setButtonColor(
            Button button,
            int buttonColor,
            int buttonTextColor,
            int buttonPressedColor,
            int buttonPressedTextColor,
            float alpha) {
        ShapeElement shapeElement = new ShapeElement();
        if (button.getBackgroundElement() != null) {
            shapeElement = (ShapeElement) button.getBackgroundElement();
        }
        RgbColor rgbColor = new RgbColor(ColorUtils.redInt(buttonColor),
                ColorUtils.greenInt(buttonColor),
                ColorUtils.blueInt(buttonColor),
                (int) (INT_255 * alpha));
        shapeElement.setRgbColor(rgbColor);
        button.setTextColor(new Color(buttonTextColor));
        button.setBackground(shapeElement);
    }

    static void slideDownIfNecessary(Component view, boolean isAnimate) {
        if (!isAnimate) {
            endPreviousAnimationIfNecessary(view);
            onSlidingFinished(view, false);

            return;
        }

        performSlideAnimation(view, false);
    }

    static void slideUpIfNecessary(Component view, boolean isAnimate) {
        if (!isAnimate) {
            endPreviousAnimationIfNecessary(view);
            onSlidingFinished(view, true);

            return;
        }

        performSlideAnimation(view, true);
    }

    /**
     * performSlideAnimation
     *
     * @param view
     * @param isSlideUp
     */
    private static void performSlideAnimation(final Component view, final boolean isSlideUp) {
        int currentHeight = view.getHeight();
        view.estimateSize(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        int expandedHeight = view.getEstimatedHeight();
        if (expandedHeight == 0) {
            expandedHeight = 53;
        }
        if (currentHeight < 0) {
            // A negative current height means the view's height hasn't been measured yet, so we
            // assign this value manually depending on whether the animation slides up or down
            currentHeight = isSlideUp ? expandedHeight : 0;
            setViewHeight(view, currentHeight);
        }

        double initialValue = (double) currentHeight / (double) expandedHeight;

        double finalValue = isSlideUp ? 0 : 1;
        double correctedInitialValue = Math.min(initialValue, 1);
        if (correctedInitialValue == finalValue) {
            // No need to animate anything because initial value and final value match
            endPreviousAnimationIfNecessary(view);
            onSlidingFinished(view, isSlideUp);
            return;
        }
        Optional<Display> optional = DisplayManager.getInstance().getDefaultDisplay(view.getContext());

        float density = optional.get().getAttributes().densityPixels;
        long durationMillis = ((int) (expandedHeight * (Math.abs(finalValue - correctedInitialValue)) / density)) * 2;
        durationMillis = Math.max(durationMillis, MIN_DURATION_MILLIS);
        AnimatorValue animator = new AnimatorValue();
        animator.setDuration(durationMillis);
        int finalExpandedHeight = expandedHeight;
        animator.setValueUpdateListener((animatorValue, v) -> {
            double value = correctedInitialValue + (finalValue - correctedInitialValue) * v;
            view.setAlpha((float) value);
            int newHeight = (int) (finalExpandedHeight * value);
            setViewHeight(view, newHeight);
        });

        animator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                view.setAlpha((float) correctedInitialValue);
                view.setVisibility(Component.VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                runningObjectAnimators.remove(view);
                onSlidingFinished(view, isSlideUp);
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        if (!runningObjectAnimators.containsKey(view)) {
            runningObjectAnimators.put(view, animator);
            animator.start();
        }
    }

    private static void onSlidingFinished(Component view, boolean isSlideUp) {
        setViewHeight(view, isSlideUp ? 0 : ComponentContainer.LayoutConfig.MATCH_CONTENT);
        view.setAlpha(isSlideUp ? 0f : 1f);
        view.setVisibility(isSlideUp ? Component.HIDE : Component.VISIBLE);
    }

    private static void endPreviousAnimationIfNecessary(Component view) {
        if (runningObjectAnimators.containsKey(view)) {
            AnimatorValue previousAnimator = runningObjectAnimators.get(view);
            if (previousAnimator != null) {
                previousAnimator.end();
            }
        }
    }

    private static void setViewHeight(Component view, int newHeight) {
        ComponentContainer.LayoutConfig layoutParams = view.getLayoutConfig();
        layoutParams.height = newHeight;
        view.setLayoutConfig(layoutParams);
        view.setHeight(newHeight);
    }
}
